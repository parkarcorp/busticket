# **Bus Ticketing IOT Project** #
+ Base URL

		http://bustcket.appspot.com/


### Insert Data:

+ URL

		http://bustcket.appspot.com/insert

+ Method
		
		POST

+ Request (application/json)

		{
			"id": "1",
			"latitude": "18.532143",
			"longitude": "73.852023",
			"type": "D"
		}

		*Type - S for source, D for destination 
		
* Response 200 (application/json)
	* Success
	
			{
				"errorCode": 0,
				"errorDesc": "Success"
			}

	* Failure
	
			{
				"errorCode": 0,
				"errorDesc": "Reason"
			}


---------------------------------------------------
### Fetch All

+ URL

		http://bustcket.appspot.com/fetchAll

+ Method

		GET
 
+ Response 200 (application/json)

		{
			"errorCode": 0,
			"errorDesc": "Success",
			"data": [{
				"userId": 3,
				"latitude": "18.532143",
				"longitude": "73.852023",
				"type": "s",
				"date": "2017-06-04",
				"time": "18:43:52"
			}]
		}

---------------------------------------------------

## Fetch Journeys

+ URL

		http://bustcket.appspot.com/fetchJourneys

+ Method

		GET
 
+ Response 200 (application/json)

		{
			"errorCode": 0,
			"errorDesc": "Success",
			"data": [{
				"userId": "3",
				"sourceLatitude": "NA",
				"sourceLongitude": "NA",
				"destinationLatitude": "NA",
				"destinationLongitude": "NA"
			}]
		}