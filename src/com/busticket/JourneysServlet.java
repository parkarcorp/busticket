package com.busticket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.busticket.datamodels.JourneyResponse;
import com.busticket.datamodels.UserData;
import com.busticket.models.PMFHttpServlet;
import com.busticket.models.ServerResponse;
import com.google.gson.Gson;

public class JourneysServlet extends PMFHttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		List<UserData> savedData = getPersistentObjects(UserData.class);
		Map<String, UserData> latestSource = new HashMap<>();
		Map<String, UserData> latestDestination = new HashMap<>();

		ServerResponse<List<JourneyResponse>> response = null;
		if (savedData == null || savedData.isEmpty()) {
			response = new ServerResponse<>(1, "No Data", null);
		} else {
			// find latest source and destination
			for (UserData data : savedData) {
				String id = data.getId();
				String type = data.getType();
				long time = Long.parseLong(data.getDateTimestamp());

				if (type.equalsIgnoreCase(UserData.TYPE_SOURCE)) {
					UserData source = latestSource.get(id);
					if (source == null) {
						latestSource.put(id, data);
					} else {
						long sourceTime = Long.parseLong(source
								.getDateTimestamp());

						if (time > sourceTime) {
							latestSource.put(id, data);
						}
					}
				} else {
					UserData destination = latestDestination.get(id);
					if (destination == null) {
						latestDestination.put(id, data);
					} else {
						long sourceTime = Long.parseLong(destination
								.getDateTimestamp());

						if (time > sourceTime) {
							latestDestination.put(id, data);
						}
					}
				}
			}

			Set<String> sourceKeys = latestSource.keySet();
			List<JourneyResponse> journeyData = new ArrayList<>(
					sourceKeys.size());

			// parse for destination
			for (String key : sourceKeys) {
				UserData source = latestSource.get(key);
				UserData dest = latestDestination.get(key);

				if (source == null || dest == null) {
					journeyData.add(new JourneyResponse(key, "NA", "NA", "NA",
							"NA"));
				} else {

					long sourceTime = Long.parseLong(source
							.getDateTimestamp());
					long destTime = Long.parseLong(dest
							.getDateTimestamp());

					if (sourceTime > destTime) {
						journeyData.add(new JourneyResponse(key, source
								.getLatitude(), source.getLongitude(), dest
								.getLatitude(), dest.getLongitude()));
					} else {
						journeyData.add(new JourneyResponse(key, "NA", "NA",
								"NA",
								"NA"));
					}
				}
			}

			response = new ServerResponse<>(0, "Success", journeyData);
		}

		resp.getWriter().write(new Gson().toJson(response));
	}
}
