package com.busticket;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.busticket.datamodels.UserData;
import com.busticket.models.PMFHttpServlet;
import com.busticket.models.ServerResponse;
import com.busticket.utils.StringUtils;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class BusTicketingServlet extends PMFHttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		String jsonRequest = getRequestString(req);

		ServerResponse<String> response = process(jsonRequest);
		resp.getWriter().write(new Gson().toJson(response));

	}

	private ServerResponse<String> process(String jsonRequest) {
		if (!StringUtils.isNullOrEmpty(jsonRequest)) {
			UserData requestData = new Gson().fromJson(jsonRequest,
					UserData.class);
			System.out.println("request : " + requestData);
			if (requestData == null) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}
			if (StringUtils.isNullOrEmpty(requestData.getId())) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}

			if (StringUtils.isNullOrEmpty(requestData.getType())) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}

			if (StringUtils.isNullOrEmpty(requestData.getLatitude())) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}

			if (StringUtils.isNullOrEmpty(requestData.getLongitude())) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}
			
			if (StringUtils.isNullOrEmpty(requestData.getBusStopName())) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}
			
			if (StringUtils.isNullOrEmpty(requestData.getPercent())) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}
			
			if (StringUtils.isNullOrEmpty(requestData.getDistance())) {
				return new ServerResponse<String>(1, "Invalid Request", null);
			}

			if (doPersistent(requestData)) {
				return new ServerResponse<String>(0, "Success", null);
			}

		}
		
		return new ServerResponse<String>(1, "Invalid Request", null);

	}
}
