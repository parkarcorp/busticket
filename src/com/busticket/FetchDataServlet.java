package com.busticket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.busticket.datamodels.UserData;
import com.busticket.datamodels.UserDataResponse;
import com.busticket.models.PMFHttpServlet;
import com.busticket.models.ServerResponse;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class FetchDataServlet extends PMFHttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		List<UserData> savedData = getPersistentObjects(UserData.class);
		
		ServerResponse<List<UserDataResponse>> response = null;
		if(savedData == null || savedData.isEmpty()) {
			response = new ServerResponse<>(1, "No Data", null);
		} else {
			List<UserDataResponse> responseData = new ArrayList<>(savedData.size());
			for(UserData data : savedData) {
				responseData.add(new UserDataResponse(data));
			}
			response = new ServerResponse<>(0, "Success", responseData);
		}
		
		
		resp.getWriter().write(new Gson().toJson(response));
	}
}
