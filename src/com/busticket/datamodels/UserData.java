package com.busticket.datamodels;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import com.google.gson.annotations.SerializedName;

@PersistenceCapable
public class UserData {
	
	public static final String TYPE_SOURCE = "S";
	public static final String TYPE_DESTINATION = "D";

	@Persistent
	private String id;
	
	@Persistent
	@SerializedName("name")
	private String busStopName;
	
	@Persistent
	private String distance;
	
	@Persistent
	private String percent;

	@Persistent
	private String latitude;

	@Persistent
	private String longitude;

	@Persistent
	private String type;

	@Persistent
	private String dateTimestamp;

	public UserData() {
		dateTimestamp = String.valueOf(System.currentTimeMillis());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDateTimestamp() {
		return dateTimestamp;
	}

	public void setDateTimestamp(String dateTimestamp) {
		this.dateTimestamp = dateTimestamp;
	}

	public String getBusStopName() {
		return busStopName;
	}

	public void setBusStopName(String busStopName) {
		this.busStopName = busStopName;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}
	
	
}
