package com.busticket.datamodels;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class UserDataResponse {

	private String userId;

	private String latitude;

	private String longitude;

	private String type;

	private String date;

	private String time;

	public UserDataResponse(UserData data) {
		userId = data.getId();
		latitude = data.getLatitude();
		longitude = data.getLongitude();
		type = data.getType();

		long currentTime = Long.parseLong(data.getDateTimestamp());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
				Locale.ENGLISH);
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss",
				Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		timeFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		cal.setTimeInMillis(currentTime);

		this.date = dateFormat.format(cal.getTime());
		this.time = timeFormat.format(cal.getTime());
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
